<main class="pattern">
        @include('includes.v2banner')
       @include('includes.v2areas')
        
        <div class="container margin_60_35">
            <div class="main_title_3">
                <span></span>
                <h2>Famous Shops</h2>
                <p>Cum doctus civibus efficiantur in imperdiet deterruisset.</p>
                <a href="grid-listings-filterscol.html">See all</a>
            </div>
            <div class="row add_bottom_30">
                <div class="col-lg-3 col-sm-6">
                     @foreach ($lodges as $lodge)
                        @include ('listings.partials.v2base_lodges', compact('lodge'))
                     @endforeach
                </div>
                
            </div>
            <!-- /row -->
            
            <div class="main_title_3">
                <span></span>
                <h2>Popular Hotels</h2>
                <p>Cum doctus civibus efficiantur in imperdiet deterruisset.</p>
                <a href="grid-listings-filterscol.html">See all</a>
            </div>
            <div class="row add_bottom_30">
                <div class="col-lg-3 col-sm-6">
                    <a href="detail-hotel.html" class="grid_item small">
                        <figure>
                            <img src="/svg/hotel.jpg" alt="">
                            <div class="info">
                                <div class="cat_star"><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i></div>
                                <h3>Hotel Mariott</h3>
                            </div>
                        </figure>
                    </a>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <a href="detail-hotel.html" class="grid_item small">
                        <figure>
                            <img src="/svg/hotel4.jpg" alt="">
                            <div class="info">
                                <div class="cat_star"><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i></div>
                                <h3>Hotel Concorde</h3>
                            </div>
                        </figure>
                    </a>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <a href="detail-hotel.html" class="grid_item small">
                        <figure>
                            <img src="/svg/hotel3.jpg" alt="">
                            <div class="info">
                                <div class="cat_star"><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i></div>
                                <h3>Hotel La Defanse</h3>
                            </div>
                        </figure>
                    </a>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <a href="detail-hotel.html" class="grid_item small">
                        <figure>
                            <img src="/svg/hetel2.jpg" alt="">
                            <div class="info">
                                <div class="cat_star"><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i></div>
                                <h3>Hotel Carlton</h3>
                            </div>
                        </figure>
                    </a>
                </div>
            </div>
            <!-- /row -->
            
            <div class="main_title_3">
                <span></span>
                <h2>Top Restaurants</h2>
                <p>Cum doctus civibus efficiantur in imperdiet deterruisset.</p>
                <a href="grid-listings-filterscol.html">See all</a>
            </div>
            <div class="row ">
                <div class="col-lg-3 col-sm-6">
                    <a href="detail-restaurant.html" class="grid_item small">
                        <figure>
                            <img src="/svg/res.jpg" alt="">
                            <div class="info">
                                <h3>Da Alfredo</h3>
                            </div>
                        </figure>
                    </a>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <a href="detail-restaurant.html" class="grid_item small">
                        <figure>
                            <img src="/svg/res2.jpg" alt="">
                            <div class="info">
                                <h3>Bistroter</h3>
                            </div>
                        </figure>
                    </a>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <a href="detail-restaurant.html" class="grid_item small">
                        <figure>
                            <img src="/svg/res3.jpg" alt="">
                            <div class="info">
                                <h3>Da Luigi</h3>
                            </div>
                        </figure>
                    </a>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <a href="detail-restaurant.html" class="grid_item small">
                        <figure>
                            <img src="/svg/res4.jpg" alt="">
                            <div class="info">
                                <h3>Marco King</h3>
                            </div>
                        </figure>
                    </a>
                </div>
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
        
        <div class="call_section">
            <div class="wrapper">
                <div class="container margin_80_55">
                    <div class="main_title_2">
                        <span><em></em></span>
                        <h2>How it Works</h2>
                        <p>Cum doctus civibus efficiantur in imperdiet deterruisset.</p>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="box_how">
                                <i class="fas fa-search"></i>
                                <h3>Search Locations</h3>
                                <p>An nec placerat repudiare scripserit, temporibus complectitur at sea, vel ignota fierent eloquentiam id.</p>
                                <span></span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="box_how">
                               <i class="fas fa-map-marker-alt"></i>
                                <h3>View Location Info</h3>
                                <p>An nec placerat repudiare scripserit, temporibus complectitur at sea, vel ignota fierent eloquentiam id.</p>
                                <span></span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="box_how">
                               <i class="fas fa-phone"></i>
                                <h3>Book, Reach or Call</h3>
                                <p>An nec placerat repudiare scripserit, temporibus complectitur at sea, vel ignota fierent eloquentiam id.</p>
                            </div>
                        </div>
                    </div>
                    <!-- /row -->
                    <p class="text-center add_top_30 wow bounceIn" data-wow-delay="0.5s"><a href="account.html" class="btn_1 rounded">Register Now</a></p>
                </div>
                <canvas id="hero-canvas" width="1920" height="480"></canvas>
            </div>
            <!-- /wrapper -->
        </div>
        <!--/call_section-->
        
       
        <!-- /container -->
    </main>
    <!-- /main -->
