 <div class="jobs-listing section-padding">
        <div class="container">
            <div class="job-topbar">
                <span class="pull-left">

                
                 </span>
                
            </div>
           

            <div class="tab-content tr-job-posted">
                <div role="tabpanel" class="tab-pane fade in active" id="four-colum">
                    <div class="row">
                        <div class="col-sm-6 col-md-3">
                            <div class="job-item">
                                <div class="item-overlay">
                                    <div class="job-info">
                                        <a href="#" class="btn btn-primary">Full Time</a>
                                        <span class="tr-title">
                                            <a href="job-details.html">Project Manager</a>
                                            <span><a href="#">Dig File</a></span>
                                        </span>
                                        <ul class="tr-list job-meta">
                                            <li><i class="fa fa-map-signs" aria-hidden="true"></i>San Francisco, CA, US</li>
                                            <li><i class="fa fa-briefcase" aria-hidden="true"></i>Mid Level</li>
                                            <li><i class="fa fa-money" aria-hidden="true"></i>$5,000 - $6,000</li>
                                        </ul>
                                        <ul class="job-social tr-list">
                                            <li><a href="#"><i class="fas fa-heart-o" aria-hidden="true"></i></a></li>
                                            <li><a href="#"><i class="fas fa-expand" aria-hidden="true"></i></a></li>
                                            <li><a href="#"><i class="fas fa-bookmark-o" aria-hidden="true"></i></a></li>
                                            <li><a href="job-details.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></a></li>
                                        </ul>
                                    </div>                                      
                                </div>
                                <div class="job-info">
                                    <div class="company-logo">
                                        <img src="/images/job/1.png" alt="images" class="img-responsive">
                                    </div>
                                    <span class="tr-title">
                                        <a href="#">Project Manager</a>
                                        <span><a href="#">Dig File</a></span>
                                    </span>
                                    <ul class="tr-list job-meta">
                                        <li><span><i class="fa fa-map-signs" aria-hidden="true"></i></span>San Francisco, CA, US</li>
                                        <li><span><i class="fa fa-briefcase" aria-hidden="true"></i></span>Mid Level</li>
                                        <li><span><i class="fa fa-money" aria-hidden="true"></i></span>$5,000 - $6,000</li>
                                    </ul>
                                    <div class="time">
                                        <a href="#"><span>Full Time</span></a>
                                        <span class="pull-right">Posted 23 hours ago</span>
                                    </div>                                                                              
                                </div>
                            </div>
                        </div>
                                   
                </div><!-- /.tab-pane -->
            </div><!-- /.tab-content -->        
        </div><!-- /.container -->      
    </div><!-- /.jobs-listing -->