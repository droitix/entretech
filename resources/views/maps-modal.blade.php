<!-- Modal -->
<div id="mapModal" class="modal fade modal-lg" role="dialog" style="width:100%;">
  
  <div class="modal-dialog" style="width:235vw;margin:auto;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <!-- <h4 class="modal-title">Modal Header</h4> -->
      </div>
      <div class="modal-body">
        <div id="map_canvas" style="height:275vh;width:100%;"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>

</div>
