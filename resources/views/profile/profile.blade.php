

@extends('layouts.v2adminapp')

@section('content')

<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Add listing</li>
      </ol>
        <div class="box_general padding_bottom">
            <div class="header_box version_2">
                <h2><i class="fas fa-user"></i>Profile details</h2>
            </div>
            <div class="row">
                <div class="col-md-4">
                  <form action="">
                    <div class="form-group">
                    <label>Your photo</label>
                      <div class="change-photo">
                                    <div class="user-image">
                                        <img src="/uploads/avatars/{{ $user->avatar }}" style="width :100px; height:100px; position:relative; border-radius: 50%; top:-10px; left:10px; margin-top: 10px;" alt="Image" class="img-responsive">
                                </div>

                       <form enctype="multipart/form-data" style="margin-bottom:150px;" action="{{route('profile.update.avatar')}}" method="POST" id="avatarForm">                            
                            <input type="file" name="avatar">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="submit" class="bt btn-sm btn-primary" style="margin-top: 20px;">
                        </form>
                      </div>
                    </div>
                </div>
                <div class="col-md-8 add_top_30">
                  <form action="{{route('profile.update')}}" method="post">
                    {{ csrf_field()  }}  
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Username</label>
                                <input type="text" name="name" class="form-control" value="{{ $user->name }}" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Full Name or Company</label>
                                 <input type="text" name="fullname" class="form-control" value="{{ $user->fullname }}" placeholder="e.g NICOZ Diamond">
                            </div>
                        </div>
                    </div>
                    <!-- /row-->
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Phone</label>
                               <input type="text" name="phone" class="form-control" value="{{ $user->phone }}"  placeholder="e.g 772543219">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" name="email" class="form-control" value="{{ $user->email }}" required>
                            </div>
                        </div>
                    </div>
                    <!-- /row-->
                   
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Facebook</label>
                                <input type="text" name="facebook" class="form-control" value="{{ $user->facebook }}" placeholder="e.g peter124">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Twitter</label>
                                <input type="text" name="twitter" class="form-control" value="{{ $user->twitter }}" placeholder="e.g jammy456">
                            </div>
                        </div>
                    </div>
                    <!-- /row-->
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Instagram</label>
                                <input type="text" name="instagram" class="form-control" value="{{ $user->instagram }}" placeholder="e.g paula2013">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Location</label>
                                <input type="text"  name="location" class="form-control" value="{{ $user->location }}" placeholder="e.g Kuwadzana,Harare,Zimbabwe">
                            </div>
                        </div>
                    </div>
                    <!-- /row-->

                    <div class="form-group" style="text-align:center;">
                                    <button type="submit" class="btn btn-sm btn-primary" style="width:50%;">Update Details</button>
                    </div>
                    
                </div>
               
          
           
           </div>
        </div>
       
       </form>
        <!-- /row-->
        
      </div>
      <!-- /.container-fluid-->
    </div>
    <!-- /.container-wrapper-->
 
@endsection