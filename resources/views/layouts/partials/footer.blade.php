
    <div class="footer">
        <div class="footer-top section-padding">
            <div class="container">
                <div class="row">S
                    <div class="col-sm-3">
                        <div class="footer-widget">
                            <h3>About Us</h3>
                            <ul class="tr-list">
                                <li><a href="#">About Ndepapi</a></li>
                                <li><a href="{{ url('/terms') }}">Terms & Conditions</a></li>
                                <li><a href="{{ url('/privacy') }}">Privacy Policy</a></li>
                                <li><a href="#">Contact Us</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="footer-widget">
                            <h3>Collaborators Accounts</h3>
                            <ul class="tr-list">
                                <li><a href="#">Create Account</a></li>
                                <li><a href="#">How to list</a></li>
                                <li><a href="#">Earning with Ndepapi</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="footer-widget">
                            <h3>Organisations</h3>
                            <ul class="tr-list">
                                <li><a href="#">Create Account</a></li>
                                <li><a href="#">Account Management</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="footer-widget">
                            <h3>Team</h3>
                            <ul class="tr-list">
                                <li><a href="{{ url('/team') }}">Ndepapi Team</a></li>
                                <li><a href="#">Careers</a></li>
                            </ul>
                        </div>
                    </div>
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div><!-- /.footer-top -->
        <div class="footer-bottom">
            <div class="container">
                <div class="copyright">
                    <p>Copyright © 2018 <a href="#">Ndepapi.co.zw.</a> All rights reserved.</p>
                </div>
                <div class="footer-social pull-right">
                    <ul class="tr-list">
                        <li><a href="#" title="Facebook"><i class="fab fa-facebook"></i></a></li>
                        <li><a href="#" title="Twitter"><i class="fab fa-twitter"></i></a></li>
                        <li><a href="#" title="Google Plus"><i class="fab fa-google-plus"></i></a></li>
                        <li><a href="#" title="Youtube"><i class="fab fa-youtube"></i></a></li>
                    </ul>
                </div>
            </div>
        </div><!-- /.footer-bottom -->
    </div><!-- /.footer -->