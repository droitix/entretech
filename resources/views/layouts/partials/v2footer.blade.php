
    <footer class="plus_border">
        <div class="container margin_60_35">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <a data-toggle="collapse" data-target="#collapse_ft_1" aria-expanded="false" aria-controls="collapse_ft_1" class="collapse_bt_mobile">
                        <h3>Quick Links</h3>
                        <div class="circle-plus closed">
                            <div class="horizontal"></div>
                            <div class="vertical"></div>
                        </div>
                    </a>
                    <div class="collapse show" id="collapse_ft_1">
                        <ul class="links">
                            <li><a href="{{ url('/about') }}">About us</a></li>  
                            <li><a href="{{ url('/faq') }}">Terms&amp; Conditions</a></li>
                            <li><a href="{{ url('/faq') }}">Privacy Policy</a></li>
                            <li><a href="{{ url('/register') }}">Create account</a></li>
                            <li><a href="{{ url('/faq') }}">FAQ</a></li>
                            <li><a href="{{ url('/contact') }}">Contact us</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <a data-toggle="collapse" data-target="#collapse_ft_2" aria-expanded="false" aria-controls="collapse_ft_2" class="collapse_bt_mobile">
                        <h3>Popular Categories</h3>
                        <div class="circle-plus closed">
                            <div class="horizontal"></div>
                            <div class="vertical"></div>
                        </div>
                    </a>
                    <div class="collapse show" id="collapse_ft_2">
                        <ul class="links">
                            <li><a href="{{ url('/zim-harare/categories/hotels-recreation-lodges/listings') }}">Lodges</a></li>
                            <li><a href="{{ url('/zim-harare/categories/food-drinks-restaurants/listings') }}">Restaurants</a></li>
                            <li><a href="{{ url('/zim-harare/categories/education-secondary/listings') }}">Schools</a></li>
                            <li><a href="{{ url('/zim-harare/categories/hotels-recreation-hotels/listings') }}">Hotels</a></li>
                            <li><a href="{{ url('/zim-harare/categories/education-universities/listings') }}">Universities</a></li>
                            <li><a href="{{ url('/zim-harare/categories/venues-wedding-venues/listings') }}">Wedding Venues</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <a data-toggle="collapse" data-target="#collapse_ft_3" aria-expanded="false" aria-controls="collapse_ft_3" class="collapse_bt_mobile">
                        <h3>Contacts</h3>
                        <div class="circle-plus closed">
                            <div class="horizontal"></div>
                            <div class="vertical"></div>
                        </div>
                    </a>
                    <div class="collapse show" id="collapse_ft_3">
                        <ul class="contacts">
                            <li><i class="fas fa-home"></i>Harare - Zimbabwe</li>
                            <li><i class="fas fa-headset"></i>+ 263719220024</li>
                            <li><i class="fas fa-envelope"></i><a href="#0">admin@ndepapi.co.zw</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <a data-toggle="collapse" data-target="#collapse_ft_4" aria-expanded="false" aria-controls="collapse_ft_4" class="collapse_bt_mobile">
                        <div class="circle-plus closed">
                            <div class="horizontal"></div>
                            <div class="vertical"></div>
                        </div>
                        <h3>Keep in touch</h3>
                    </a>
                    <div class="collapse show" id="collapse_ft_4">
                        <div id="newsletter">
                            <div id="message-newsletter"></div>
                            <form method="post" action="assets/newsletter.php" name="newsletter_form" id="newsletter_form">
                                <div class="form-group">
                                    <input type="email" name="email_newsletter" id="email_newsletter" class="form-control" placeholder="Your email">
                                    <input type="submit" value="Submit" id="submit-newsletter">
                                </div>
                            </form>
                        </div>
                        <div class="follow_us">
                            <h5>Follow Us</h5>
                            <ul>
                                <li><a href="https://www.facebook.com/ndepapi"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="https://twitter.com/ndepapilistings"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="https://www.instagram.com/ndepapilistings"><i class="fab fa-instagram"></i></a></li>
                                
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /row-->
            <hr>
            <div class="row">
                <div class="col-lg-6">
                    <ul id="footer-selector">
                        <li>
                            
                               
                            
                        </li>
                      
                    </ul>
                </div>
                <div class="col-lg-6">
                    <ul id="additional_links">
                        <li><a href="{{ url('/faq') }}">Terms and conditions</a></li>
                        <li><a href="{{ url('/faq') }}">Privacy</a></li>
                        <li><span>© 2018 Ndepapi</span></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
    <!--/footer-->