<!DOCTYPE html>
<html lang="en">
<head>

   @include('layouts.partials.v2headslider')
 
</head>
<body>
 <div id="page" class="theia-exception">
   @include('layouts.partials.v2navigationlist')

    @include('layouts.partials._alerts')
    
       @yield('content')



   @include('layouts.partials.v2footer')
  </div>
     
    
    @include('layouts.partials.v2signinpopup')

    <div id="toTop"></div><!-- Back to top button -->
    
    <!-- COMMON SCRIPTS -->
    <script src="js/common_scripts.js"></script>
    <script src="js/functions.js"></script>
    <script src="assets/validate.js"></script>
    
    <!-- Map -->
    <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBnG1RgQ8jpVgciCQQyuj50URjMIVl9kDo"></script>
    <script src="js/map_single_hotel.js"></script>
    <script src="js/infobox.js"></script>
    
    <!-- INPUT QUANTITY  -->
    <script src="js/input_qty.js"></script>
    
    <!-- DATEPICKER  -->
    <script>
    $(function() {
      $('input[name="dates"]').daterangepicker({
          autoUpdateInput: false,
          parentEl:'#input-dates',
          opens: 'left',
          locale: {
              cancelLabel: 'Clear'
          }
      });
      $('input[name="dates"]').on('apply.daterangepicker', function(ev, picker) {
          $(this).val(picker.startDate.format('MM-DD-YY') + ' > ' + picker.endDate.format('MM-DD-YY'));
      });
      $('input[name="dates"]').on('cancel.daterangepicker', function(ev, picker) {
          $(this).val('');
      });
    });
    </script>
</body>
</html>
