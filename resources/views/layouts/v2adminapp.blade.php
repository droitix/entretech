<!DOCTYPE html>
<html lang="en">
<head>

   @include('layouts.partials.v2adminhead')
 
</head>
<body>
 <body class="fixed-nav sticky-footer" id="page-top">
          
   @include('layouts.partials.v2adminnavigation')

    @include('layouts.partials._alerts')
    
       @yield('content')



   
           

  </div>
     
     @include('layouts.partials.v2adminfooter') 
    

    <div id="toTop"></div><!-- Back to top button -->
   <!-- Bootstrap core JavaScript-->
    <script src="/2.0/vendor/jquery.min.js"></script>
    <script src="/2.0/vendor/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="/2.0/vendor/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <script src="/2.0/vendor/Chart.js"></script>
    <script src="/2.0/vendor/jquery.dataTables.js"></script>
    <script src="/2.0/vendor/dataTables.bootstrap4.js"></script>
    <script src="/2.0/vendor/jquery.selectbox-0.2.js"></script>
    <script src="/2.0/vendor/retina-replace.min.js"></script>
    <script src="/2.0/vendor/jquery.magnific-popup.min.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="js/admin.js"></script>
    <!-- Custom scripts for this page-->
    <script src="js/admin-charts.js"></script>
</body>
</html>
