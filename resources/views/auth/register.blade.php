@extends('layouts.v2authapp')

@section('content')

   <body id="register_bg">
    
    <nav id="menu" class="fake_menu"></nav>
    
    <div id="login">
        <aside>
            <figure>
                <a href="{{ url('/') }}"><img src="/svg/logo.png" width="125" height="35" alt="" class="logo_sticky"></a>
            </figure>
            <div class="divider"><span>Create an Account</span></div>
            <form action="{{ route('register') }}" method="POST" autocomplete="off">
                 @csrf
                <div class="form-group">
                    <label>Your Name or Company</label>
                    <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus placeholder="Username">
                           <i class="fas fa-user"></i>
                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                    
                </div>
                <div class="form-group">
                    <label>Your Email</label>
                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required placeholder="Email">
                                 <i class="fas fa-envelope"></i>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                   
                </div>
                <div class="form-group">
                    <label>Your Password (5 or more characters)</label>
                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Choose a password">
                           
                             <i class="fas fa-lock"></i>
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                   
                </div>
                <div class="form-group">
                    <label>Repeat password</label>
                     <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required placeholder="Type password again">
                    <i class="fas fa-lock"></i>
                </div>
               
                <div id="pass-info" class="clearfix"></div>
                <button type="submit" class="btn_1 rounded full-width add_top_30">Register Now!</button>
               
                <div class="text-center add_top_10">Already have an acccount? <strong><a href="{{ url('/login') }}">Sign In</a></strong></div>
            </form>
            <div class="copy">© 2018 Ndepapi</div>
        </aside>
    </div>
    <!-- /login -->
@endsection
