@extends('layouts.appupload')

@section('edit-page-styles-scripts')

<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" media="all" rel="stylesheet" type="text/css"/>


@endsection

@section('content')
    <!-- main -->
  
        <div class="tr-breadcrumb bg-image section-before">
        <div class="container">
            <div class="breadcrumb-info text-center">
                <div class="page-title">
                    <h1>Create a listing</h1>
                </div>      
                   
            </div>
        </div><!-- /.container -->
    </div><!-- /.tr-breadcrumb -->
       <div class="tr-post-job page-content">
        <div class="container">
            <div class="row">
                <div class="col-sm-9">
                    <div class="short-info code-edit-small">
                        <div class="section">
                            <span class="tr-title">Listing Information (Please  upload quality images)</span>
                            <form action="{{ route('listings.store', [$area]) }}" method="post">
                                  <input type="hidden" name="image_names" id="image_names">
                  <fieldset>              
                    <div class="row">
                        @include('listings.partials.forms._areas')
                         @include('listings.partials.forms._categories')
                    </div>
                                    <div class="row">
                        <div class="form-group{{ $errors->has('companyname') ? ' has-error' : '' }} col-md-8">
                            <label for="companyname" class="control-label">Company or Organisation name</label>
                            <input type="text" name="companyname" id="companyname" class="form-control" placeholder="e.g Muzinda Lodge">

                            @if ($errors->has('companyname'))
                                <span class="help-block">
                                    {{ $errors->first('companyname') }}
                                </span>
                            @endif
                        </div>
                        </div>

                         <div class="row">
                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }} col-md-11">
                            <label for="title" class="control-label">Describe your business, organisation or location in a few lines</label>
                            <input type="text" name="title" id="title" class="form-control" placeholder="">

                            @if ($errors->has('title'))
                                <span class="help-block">
                                    {{ $errors->first('title') }}
                                </span>
                            @endif
                        </div>
                    </div>

                      <div class="row">
                  <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }} col-md-6">
                            <label for="address" class="control-label">Address or Location</label>
                            <input type="text" name="address" id="address" class="form-control" placeholder="e.g 34 Sterling Road, Eastlea">

                            @if ($errors->has('address'))
                                <span class="help-block">
                                    {{ $errors->first('address') }}
                                </span>
                            @endif
                        </div>
                    </div>

                                    <div class="row form-group add-image">
                                        <label class="col-sm-3 label-title">Listing Photos <span></span> </label>
                                        <div class="col-sm-9">
                                            <h5><i class="fa fa-upload" aria-hidden="true"></i>Select Files to Upload / Drag and Drop Files <span>You can add multiple images.</span></h5>
                                            <div class="upload-section">

                                                <div class="form-group" style="margin-top:10px">
                                                    <label for="listing_image" style="text-align:center;"><font color="brown">**Images are required and a minimum of 300 x 300pixels is expected</font></label>
                                                
                                                    <div class="form-group">
                                                        <div class="file-loading">
                                                            <input id="listing_image" type="file" name="listing_image[]" multiple class="file" data-overwrite-initial="false">
                                                        </div>
                                                    </div>
                                                </div> 

                                            </div>  
                                        </div>
                                    </div>
                                    <div class="row">
                        <div class="form-group{{ $errors->has('phone1') ? ' has-error' : '' }} col-md-4">
                            <label for="phone1" class="control-label">phone 1 (at least one phone)</label>
                            <input type="text" name="phone1" id="phone1" class="form-control" placeholder="e.g 775291423 without 0">

                            @if ($errors->has('phone1'))
                                <span class="help-block">
                                    {{ $errors->first('phone1') }}
                                </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('phone2') ? ' has-error' : '' }} col-md-4">
                            <label for="phone1" class="control-label">phone 2</label>
                            <input type="text" name="phone2" id="phone2" class="form-control" >

                            @if ($errors->has('phone2'))
                                <span class="help-block">
                                    {{ $errors->first('phone2') }}
                                </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('phone3') ? ' has-error' : '' }} col-md-4">
                            <label for="phone3" class="control-label">phone 3</label>
                            <input type="text" name="phone3" id="phone3" class="form-control" >

                            @if ($errors->has('phone3'))
                                <span class="help-block">
                                    {{ $errors->first('phone3') }}
                                </span>
                            @endif
                        </div>
                        </div>

                         <div class="row">
                        <div class="form-group{{ $errors->has('companyemail') ? ' has-error' : '' }} col-md-6">
                            <label for="companyemail" class="control-label">Email</label>
                            <input type="text" name="companyemail" id="companyemail" class="form-control" placeholder="e.g apple@gmail.com">

                            @if ($errors->has('companyemail'))
                                <span class="help-block">
                                    {{ $errors->first('companyemail') }}
                                </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('website') ? ' has-error' : '' }} col-md-5">
                            <label for="website" class="control-label">Website</label>
                            <input type="text" name="website" id="website" class="form-control" placeholder="e.g https://ndepapi.com">

                            @if ($errors->has('website'))
                                <span class="help-block">
                                    {{ $errors->first('website') }}
                                </span>
                            @endif
                        </div>
                        </div>
                             
                        <div class="row">
                        <div class="form-group{{ $errors->has('facebook') ? ' has-error' : '' }} col-md-4">
                            <label for="facebook" class="control-label">Facebook</label>
                            <input type="text" name="facebook" id="facebook" class="form-control" placeholder="e.g https://facebook.com/paulkumbweya">

                            @if ($errors->has('facebook'))
                                <span class="help-block">
                                    {{ $errors->first('facebook') }}
                                </span>
                            @endif
                        </div>

                       <div class="form-group{{ $errors->has('twitter') ? ' has-error' : '' }} col-md-4">
                            <label for="twitter" class="control-label">Twitter</label>
                            <input type="text" name="twitter" id="twitter" class="form-control" placeholder="e.g https://twitter.com/paulkumz">

                            @if ($errors->has('twitter'))
                                <span class="help-block">
                                    {{ $errors->first('twitter') }}
                                </span>
                            @endif
                        </div>


                        <div class="form-group{{ $errors->has('instagram') ? ' has-error' : '' }} col-md-4">
                            <label for="instagram" class="control-label">Instagram</label>
                            <input type="text" name="instagram" id="instagram" class="form-control" placeholder="e.g https://instagram.com/paulkumz">

                            @if ($errors->has('instagram'))
                                <span class="help-block">
                                    {{ $errors->first('instagram') }}
                                </span>
                            @endif
                        </div>

                        </div>

                     <div class="form-group{{ $errors->has('body') ? ' has-error' : '' }}">
                            <label for="body" class="control-label">Full company or Organisation description**</label>
                            <textarea name="body" id="body" cols="30" rows="8" class="form-control"></textarea>

                            @if ($errors->has('body'))
                                <span class="help-block">
                                    {{ $errors->first('body') }}
                                </span>
                            @endif
                        </div>
                                                              
                                </div><!-- section -->
                                
                                
                                <div class="checkbox section agreement">
                                    <div class="row section agreement">
                        Continue provided you agree to our <a href="{{ url('/terms') }}">Terms of Use</a> and <a href="{{ url('/privacy') }}">Privacy Policy</a> and acknowledge that you are posting authentic infrmation. 
                        </div>
                                   <div class="form-group">
                            <button type="submit" class="btn btn-primary">Post listing</button>
                        </div>
                                </div><!-- section -->
                                
                            </fieldset>
                           

                        {{ csrf_field() }}
                        </form><!-- form -->    
                    </div>
                </div>

                    <!-- quick-rules -->    
                     <div class="col-sm-3">
                    <div class="section quick-rules">
                        <span class="tr-title">Quick Posting Rules</span>
                        <p>Posting a listing on <a href="#">ndepapi.com</a> is free! However, all posts must abide with our rules:</p>
                        <ul class="tr-list">
                            <li>Make sure you post in the correct category.</li>
                            <li>Do not post the same ad more than once or repost an ad within 48 hours.</li>
                            <li>Dont duplicate ads in different categories.</li>
                            <li>Quality pictures are always loved here.</li>
                            <li>Do not put your email or phone numbers in the title or description.</li>
                        </ul>
                    </div>
                </div>             
            </div>  
        </div><!-- container -->
    </section><!-- main -->
    


        <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/js/fileinput.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/themes/fa/theme.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" type="text/javascript"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" type="text/javascript"></script>
    
    <script type="text/javascript">

        let userId = {{ \Auth::user()->id }}
        console.log(userId);
        let listingImages = $("#listing_image");
            
        $('.remove-image').on('click', function(e){
            
            $('#removed_images').val(function(i, val) {
                return val + e.target.dataset.imagePath + '|';
            });
            $(this).parents('.item-image.item-thumbnail').remove();
            listingImages.fileinput('refresh');
        });
            
        listingImages.fileinput({
            theme: 'fa',
            uploadUrl: `/upload-image/${userId}`,
            uploadAsync: false,
            showUpload: false, // hide upload button
            showRemove: false, // hide remove button
            validateInitialCount: true,
            overwriteInitial: false,
            // minFileCount: 0,
            maxFileCount: 1,
            initialPreviewAsData: true,
            uploadExtraData: function() {
                return {
                    _token: $("input[name='_token']").val(),
                };
            },
        }).on("filebatchselected", function(event, files) {
            // console.log(event, files);
            listingImages.fileinput("upload");
        });

        // CATCH RESPONSE
        listingImages.on('filebatchuploaderror', function(event, data, previewId, index) {
            var form = data.form, files = data.files, extra = data.extra, 
            response = data.response, reader = data.reader;
        });

        listingImages.on('filebatchuploadsuccess', function(event, data, previewId, index) {
            if(data.jqXHR.responseJSON){
                response = data.jqXHR.responseJSON;
                if(data.jqXHR.responseJSON.success == true){
                    $("#image_names").val(function(i, val) {
                        return val += data.jqXHR.responseJSON.imagePaths + '|';
                    });
                }
            }
        });

    </script>

@endsection
