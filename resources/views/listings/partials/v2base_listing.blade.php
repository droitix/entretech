                <div class="col-xl-4 col-lg-6 col-md-6">
                    <div class="strip grid">
                        @if(count($listing->images) > 0)
                        <figure>
                             @if (Auth::check())
                                 @if (!$listing->favouritedBy(Auth::user()))
                            <a href="#0" onclick="event.preventDefault(); document.getElementById('listings-favourite-form').submit();" class="wish_bt"></a>

                            <form action="{{ route('listings.favourites.store', [$area, $listing]) }}" method="post" id="listings-favourite-form" class="hidden">
                                        {{ csrf_field() }}
                            </form>
                             @else
                              <a href="#0" onclick="event.preventDefault(); document.getElementById('listings-destroy-form-{{ $listing->id }}').submit();" class="wish_bt liked"></a>

                              <form action="{{ route('listings.favourites.destroy', [$area, $listing]) }}" method="post" id="listings-favourites-destroy-{{ $listing->id }}">
                               {{ csrf_field() }}
                               {{ method_field('DELETE') }}
                              </form>
                            @endif
                            @else
                            <a href="{{ url('/login') }}" class="loginn_bt"></a>
                            @endif
                            <a href="{{ route('listings.show', [$area, $listing]) }}"><img src="{{'/storage/'.$listing->images[0]}}" class="img-fluid" alt=""><div class="read_more"><span>View Details</span></div></a>
                            <small>{{ $listing->area->name }}</small>
                        </figure>
                        @else
                        <figure>
                            <a href="#0" class="wish_bt"></a>
                            <a href="{{ route('listings.show', [$area, $listing]) }}"><img src="" class="img-fluid" alt=""><div class="read_more"><span>Read more</span></div></a>
                            <small>{{ $listing->area->name }}</small>
                        </figure>
                        @endif
                        <div class="wrapper">
                            <h3><a href="{{ route('listings.show', [$area, $listing]) }}">{{ $listing->companyname }}</a></h3>
                            <small>{{ $listing->address }}</small>
                            
                            <a class="address" href="{{ route('listings.show', [$area, $listing]) }}">Get directions</a>
                        </div>
                        <ul>
                            <li><span class="loc_open">in {{ $listing->category->name }}</span></li>
                            <li><div class="score"><span>Verified<em></em></span><strong><i class="fas fa-check"></i></strong></div></li>
                        </ul>
                    </div>
                </div>
                <!-- /strip grid -->