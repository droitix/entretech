            <div class="col-lg-3 col-sm-6">
                    <a href="{{ route('listings.show', [$area, $listing]) }}" class="grid_item small">
                        <figure>
                            <img src="{{'/storage/'.$listing->images[0]}}" alt="">
                            <div class="info">
                                <h3>{{ $listing->companyname }}</h3>
                                <small style="background-color: #5E4352">in {{ $listing->area->name }}, {{ $listing->area->parent->name }}</small>
                            </div>
                        </figure>
                    </a>
                </div>