   <div class="col-lg-3 col-sm-6">
                    <a href="{{ route('listings.show', [$area, $school]) }}" class="grid_item small">
                        <figure>

                            <img src="{{'/storage/'.$school->images[0]}}" alt="">
                            <div class="info">
                                <h3>{{ $school->companyname }}</h3>
                                <small>in {{ $school->area->name }}, {{ $school->area->parent->name }}</small>
                            </div>
                        </figure>
                    </a>
                </div>