@extends('layouts.app')

@section('content')
    <div class="row">
        @if (Auth::check())
            <div class="col-md-3">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <nav class="nav nav-stacked">
                            <li><a href="{{ route('listings.share.index', [$area, $listing]) }}">Email to a friend</a></li>
                            @if (!$listing->favouritedBy(Auth::user()))
                                <li>
                                    <a href="#" onclick="event.preventDefault(); document.getElementById('listings-favourite-form').submit();">Add to favourites</a>

                                    <form action="{{ route('listings.favourites.store', [$area, $listing]) }}" method="post" id="listings-favourite-form" class="hidden">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            @endif
                        </nav>
                    </div>
                </div>
            </div>
        @endif
        <div class="{{ Auth::check() ? 'col-md-9' : 'col-md-12' }}">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>{{ $listing->title }} in <span class="text-muted">{{ $listing->area->name }}</span></h4>
                </div>
                <div class="panel-body">
                    {!! nl2br(e($listing->body)) !!}
                    <hr>
                    <p>Viewed {{ $listing->views() }} times</p>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    Contact {{ $listing->user->name }}
                </div>
                <div class="panel-body">
                    @if (Auth::guest())
                        <p><a href="/register">Sign up</a> for an account or <a href="/login">sign in</a> to contact listing owners.</p>
                    @else
                        <form action="{{ route('listings.contact.store', [$area, $listing]) }}" method="post">
                            <div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">
                                <label for="message" class="control-label">Message</label>
                                <textarea name="message" id="message" cols="30" rows="5" class="form-control"></textarea>

                                @if ($errors->has('message'))
                                    <span class="help-block">
                                        {{ $errors->first('message') }}
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-default">Send</button>
                                <span class="help-block">
                                    This will email {{ $listing->user->name }} and they'll be able to reply directly to you by email.
                                </span>
                            </div>

                            {{ csrf_field() }}
                        </form>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection





 <!-- short-info -->
                            <div class="short-info">
                                <h4>Contact info</h4>
                                <p><strong>Email: </strong><a href="#">bookings@muzinda.com</a> </p>
                                <p><strong>Phones: </strong><a href="#">+26377345890, +263567854</a> </p>
                                <p><strong>Website: </strong><a href="#">www.muzinda.com</p>
                                <p><strong>Social Accounts:  <span class="icon"><i class="fab fa-facebook-square " style="color:blue"></i><i class="fab fa-instagram " style="color:brown"></i><i class="fab fa-twitter-square " style="color:skyblue"></i></span>
</p>
                            </div><!-- short-info -->



                            <a href=""><i class="fas fa-heart fa-2x" style="color:red"></i> Add to favourites</a>

                            &nbsp&nbsp<span><a href=""><i class="fas fa-envelope fa-2x"></i>Send Email</a></span>
                            <!-- contact-with -->
                           
                            
                            <!-- social-links -->
                            <div class="social-links">
                                <h4>Share this listing</h4>
                                <ul class="list-inline">
                                    <li><a href="#"><i class="fab fa-facebook-square"></i></a></li>
                                    <li><a href="#"><i class="fab fa-twitter-square"></i></a></li>
                                    <li><a href="#"><i class="fab fa-google-plus-square"></i></a></li>
                                    <li><a href="#"><i class="fab fa-linkedin-square"></i></a></li>
                                    <li><a href="#"><i class="fa fa-pinterest-square"></i></a></li>
                                    <li><a href="#"><i class="fa fa-tumblr-square"></i></a></li>
                                </ul>
                            </div><!-- social-links -->      