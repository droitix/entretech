
<body style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; background-color: #f5f8fa; color: #74787E; height: 100%; hyphens: auto; line-height: 1.4; margin: 0; -moz-hyphens: auto; -ms-word-break: break-all; width: 100% !important; -webkit-hyphens: auto; -webkit-text-size-adjust: none; word-break: break-word; margin-left: 10vw; margin-top: 10vw;">
    <style>
        @media  only screen and (max-width: 600px) {
            .inner-body {
                width: 100% !important;
                margin-left: 5vw;
                margin-right: 5vw;
            }

            .footer {
                width: 100% !important;
                 margin-left: 5vw;
                margin-right: 5vw;
            }
        }

        @media  only screen and (max-width: 500px) {
            .button {
                width: 100% !important;
                 margin-left: 5vw;
                margin-right: 5vw;
            }
        }
    </style>
    <td class="header" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; padding: 25px 0; text-align: center;">
        <a href="http://localhost" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: #bbbfc3; font-size: 19px; font-weight: bold; text-decoration: none; text-shadow: 0 1px 0 white;">
            Ndepapi listings
        </a>
    </td><br><br>

Hi {{ $listing->user->name }}!  <br/><br/>   

The following is a reply from {{ $sender->name }} to your "<a href="{{ route('listings.show', [$listing->area, $listing]) }}">{{ $listing->companyname }}</a>." Listing on <a href="https://ndepapi.co.zw">Ndepapi</a>: <br/><br/> 

<table border="1" width="70%" bordercolor="black"  cellpadding="12" cellspacing="0">
<tr><td><b>From:</b>{{ $sender->name }}<p>
<b>Email:</b> {{ $sender->email }}<p>


<tr><td><b>Message: </b><p>{!! nl2br(e($body)) !!}</td></tr>
</table> <br/><br/>

<b>You can respond to "{{ $sender->name }}" by replying to this email. <br/><br/></b>

<b>NDEPAPI LISTINGS</b><br/><br/> 
<ul><li>We will help you advertise your business, organisation ,facility  in a shopfront format
<li>Listing on Ndepapi is currently free and you need to take advantage of that
<li>Just open an account today and start listing all your businesses or locations,specific to the area were you are<br/><br/>
</ul>
 
Please report any suspicious emails to our friendly support staff.<br/><br/>
 
The Ndepapi team

<br/><br/><br/>


------------------------<br/>
AD DETAILS<br/>
-----------------------<br/>
Title: {{$listing->companyname}}<br/>
Category:{{ $listing->category->name }}<br/>
Listed Since: {{ $listing->created_at }}<br/>
Description: {{ $listing->title }}<br/>
Link:<a href="{{ route('listings.show', [$listing->area, $listing]) }}">{{ $listing->companyname }}</a><br>
On:www.ndepapi.co.zw</a><br/>
Listing ID: {{ $listing->id }}<br/>
</body