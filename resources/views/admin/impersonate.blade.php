@extends('layouts.app')

@section('content')
 
<div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default" style="margin-top:40px;">
                   
                    <div class="panel-body">
                        <form action="{{ route('admin.impersonate') }}" method="POST">
             
          <div class="form-group">
            <label for="email">Email address</label>
            <input type="text" class="form-control" name="email" id="email">
           </div> 

           <button type="submit" class="btn btn-primary">Impersonate</button>

           {{ csrf_field() }}
  </form> 
                    </div>
                </div>
            </div>
        </div>
    </div>
 
@endsection
