<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [
                'name' => 'Events',
                'icon' => 'fa-bell',
                'color' => 'CC5803',
                'children' => [
                    ['name' => 'Sporting'],
                    ['name' => 'Music & Shows'],
                    ['name' => 'Church & Religion'],
                    ['name' => 'Professional events'],
                    ['name' => 'Social Gatherings'],
                    ['name' => 'Fund Raising'],
                    
                ]
            ],
            [
                'name' => 'Hotels & Recreation',
                'icon' => 'fa-hotel',
                'color' => '593A41',
                'children' => [
                    ['name' => 'Hotels'],
                    ['name' => 'Lodges'],
                    ['name' => 'Parks'],
                    ['name' => 'Monuments'],
                    ['name' => 'Resorts'],
                    ['name' => 'Gardens'],
                    
                ]
            ],
            [
                'name' => 'Education',
                'icon' => 'fa-graduation-cap',
                'color' => '9FA4C4',
                'children' => [
                    ['name' => 'ECD'],
                    ['name' => 'Primary'],
                    ['name' => 'Secondary'],
                    ['name' => 'Polytechnic'],
                    ['name' => 'Colleges'],
                    ['name' => 'Universities'],
                  ]
            ],
            
            [
                'name' => 'Venues',
                'icon' => 'fa-calendar-alt',
                'color' => '66821E',
                'children' => [
                    ['name' => 'Wedding Venues'],
                    ['name' => 'Church & Seminars'],
                    ['name' => 'Video & Film'],
                    ['name' => 'Open Spaces'],
                    ['name' => 'Large Gatherings'],
                    ['name' => 'Multipurpose'],
                   
                ]
            ],
             [
                'name' => 'Health',
                'icon' => 'fa-medkit',
                'color' => 'FB8B24',
                'children' => [
                    ['name' => 'Clinics'],
                    ['name' => 'Hospitals'],
                    ['name' => 'Pharmacies'],
                    ['name' => 'New Start Centers'],
                    ['name' => 'Private Hospitals'],
                    ['name' => 'Traditional Herbal Facilities'],
                   
                ]
            ],
            [
                'name' => 'Food & Drinks',
                'icon' => 'fa-wine-glass-alt',
                'color' => 'F7CB15',
                'children' => [
                    ['name' => 'Take-Aways'],
                    ['name' => 'Restaurants'],
                    ['name' => 'Coffee Shops'],
                    ['name' => 'Drive Thru'],
                    ['name' => 'Bars'],
                    ['name' => 'Delivery Services'],
                   
                ]
            ],
            [
                'name' => 'Shopping',
                'icon' => 'fa-shopping-bag',
                'color' => 'CA3C25',
                'children' => [
                    ['name' => 'Malls'],
                    ['name' => 'Supermakets & Wholesales'],
                    ['name' => 'Clothing Shops'],
                    ['name' => 'Furniture & Electronics'],
                    ['name' => 'Car Sales'],
                    ['name' => 'Auction Houses'],
                   
                ]
            ],
            [
                'name' => 'Banking',
                'icon' => 'fa-university',
                'color' => '002A32',
                'children' => [
                    ['name' => 'Banks'],
                    ['name' => 'ATM'],
                    ['name' => 'Loans & Credit'],
                    ['name' => 'Mobile Money Agents'],
                    ['name' => 'Intl Money Services'],
                    ['name' => 'Multipurpose'],
                   
                ]
            ],
            [
                'name' => 'Law & Order',
                'icon' => 'fa-gavel',
                'color' => 'F2E94E',
                'children' => [
                    ['name' => 'Police Stations'],
                    ['name' => 'Law Firms'],
                    ['name' => 'Courts'],
                    ['name' => 'Private Investigators'],
                    ['name' => 'Prisons & Holding'],
                    ['name' => 'Army Barracks'],
                   
                ]
            ],
            [
                'name' => 'Travel & Logistics',
                'icon' => 'fa-plane-departure',
                'color' => '034C3C',
                'children' => [
                    ['name' => 'Taxis'],
                    ['name' => 'Car Hire'],
                    ['name' => 'Travel & Tours Services'],
                    ['name' => 'Delivery Services'],
                    ['name' => 'Buses & Bookings'],
                    ['name' => 'Airlines'],
                   
                ]
            ],

             [
                'name' => 'Proffesional Services',
                'icon' => 'fa-highlighter',
                'color' => '2E294E',
                'children' => [
                    ['name' => 'Advertising & Marketing'],
                    ['name' => 'Engineering & Architecture'],
                    ['name' => 'International logistics'],
                    ['name' => 'Security'],
                    ['name' => 'Events planning'],
                    ['name' => 'Branding'],
                   
                ]
            ],
            [
                'name' => 'Ranks & Airports',
                'icon' => 'fa-subway',
                'color' => '99C24D',
                'children' => [
                    ['name' => 'Airpots'],
                    ['name' => 'Kombi Ranks'],
                    ['name' => 'Train Stations'],
                    ['name' => 'Bus Ranks'],
                    ['name' => 'Parking Lots'],
                    ['name' => 'Other'],
                   
                ]
            ],
            [
                'name' => 'Computers & Internet',
                'icon' => 'fa-laptop',
                'color' => '99582A',
                'children' => [
                    ['name' => 'Web Hosting'],
                    ['name' => 'Web & Graphic Design'],
                    ['name' => 'Internet Cafes'],
                    ['name' => 'Telecomunications'],
                    ['name' => 'Installations'],
                    ['name' => 'Other'],
                   
                ]
            ],
            [
                'name' => 'Financial Services',
                'icon' => 'fa-hand-holding-usd',
                'color' => '390040',
                'children' => [
                    ['name' => 'Tax Services'],
                    ['name' => 'Real Estate'],
                    ['name' => 'Insurance'],
                    ['name' => 'Auditors'],
                    ['name' => 'Marketing'],
                    ['name' => 'Debt Collection'],
                   
                ]
            ],
            [
                'name' => 'Sports & Fun',
                'icon' => 'fa-football-ball',
                'color' => 'FF785A',
                'children' => [
                    ['name' => 'Stadiums'],
                    ['name' => 'Sports Arena & Pools'],
                    ['name' => 'Golf Courses'],
                    ['name' => 'Race courses'],
                    ['name' => 'Gym & Fitness'],
                    ['name' => 'Amusement & Theme Parks'],
                   
                ]
            ],
            [
                'name' => 'Entertainment',
                'icon' => 'fa-film',
                'color' => '5A2328',
                'children' => [
                    ['name' => 'Movie Theatres'],
                    ['name' => 'Lounges'],
                    ['name' => 'Gaming Centers'],
                    ['name' => 'Movie Production'],
                    ['name' => 'Drama & Poetry'],
                    ['name' => 'Shows'],
                   
                ]
            ],
            [
                'name' => 'Religion',
                'icon' => 'fa-church',
                'color' => '454955',
                'children' => [
                    ['name' => 'Pentecostal Churches'],
                    ['name' => 'Mainline Churches'],
                    ['name' => 'Mosques'],
                    ['name' => 'Synagogues'],
                    ['name' => 'Prophetic & Healing Services'],
                    ['name' => 'Religious Collections'],
                   
                ]
            ],
            [
                'name' => 'Industries',
                'icon' => 'fa-industry',
                'color' => '00120B',
                'children' => [
                    ['name' => 'Manufacturing'],
                    ['name' => 'Mining & Engineering'],
                    ['name' => 'Agricultural'],
                    ['name' => 'Clothing'],
                    ['name' => 'Building'],
                    ['name' => 'Telecomunications'],
                   
                ]
            ],

            [
                'name' => 'Services for Hire',
                'icon' => 'fa-broom',
                'color' => 'E9EB87',
                'children' => [
                    ['name' => 'Landscaping & Gardening'],
                    ['name' => 'Building & Construction'],
                    ['name' => 'Video & Photography'],
                    ['name' => 'Learning & Tutoring'],
                    ['name' => 'Mechanics & Garages'],
                    ['name' => 'Childcare & Nanny'],
                   
                ]
            ],
            [
                'name' => 'Sites',
                'icon' => 'fa-archway',
                'color' => '6B2737',
                'children' => [
                    ['name' => 'Mines'],
                    ['name' => 'Dams & Rivers'],
                    ['name' => 'Historical Sites'],
                    ['name' => 'Caves'],
                    ['name' => 'Mountains'],
                    ['name' => 'Important Locations'],
                   
                ]
            ],
        ];

        foreach ($categories as $category) {
            \App\Category::create($category);
        }
    }
}
