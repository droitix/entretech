<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOpeningtimeToListingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('listings', function (Blueprint $table) {
            $table->string('monopening')->nullable();
            $table->string('monclosing')->nullable();
            $table->string('tuesopening')->nullable();
            $table->string('tuesclosing')->nullable();
            $table->string('wedopening')->nullable();
            $table->string('wedclosing')->nullable();
            $table->string('thursopening')->nullable();
            $table->string('thursclosing')->nullable();
            $table->string('fridayopening')->nullable();
            $table->string('fridayclosing')->nullable();
            $table->string('surtopening')->nullable();
            $table->string('surtclosing')->nullable();
            $table->string('sunopening')->nullable();
            $table->string('sunclosing')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('listings', function (Blueprint $table) {
            //
        });
    }
}
