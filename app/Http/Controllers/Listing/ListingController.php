<?php

namespace App\Http\Controllers\Listing;

use App\{Area, Category, Listing};
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use App\Jobs\UserViewedListing;
use App\Http\Requests\StoreListingFormRequest;

class ListingController extends Controller
{
    public function index(Area $area, Category $category)
    {
        $listings = Listing::with(['user', 'area'])->isLive()->inArea($area)->fromCategory($category)->latestFirst()->paginate(15);



        return view('listings.display.index3', compact('listings', 'category'));

    }

  

    public function show(Request $request, Area $area, Listing $listing)
    {
        if (!$listing->live()) {
            abort(404);
        }

        if ($request->user()) {
            dispatch(new UserViewedListing($request->user(), $listing));
        }

        return view('listings.v2show2', compact('listing'));
    }

    public function create()
    {
        return view('listings.v2create');
    }

    public function store(StoreListingFormRequest $request, Area $area)
    {
        $listing = new Listing;
        $listing->title = $request->title;
        $listing->companyname = $request->companyname;
        $listing->companyemail = $request->companyemail;
        $listing->address = $request->address;
        $listing->facebook = $request->facebook;
        $listing->twitter = $request->twitter;
        $listing->instagram = $request->instagram;
        $listing->website = $request->website;
        $listing->phone1 = $request->phone1;
        $listing->phone2 = $request->phone2;
        $listing->phone3 = $request->phone3;
        $listing->body = $request->body;
        $listing->monopening = $request->monopening;
        $listing->monclosing = $request->monclosing;
        $listing->tuesopening = $request->tuesopening;
        $listing->tuesclosing = $request->tuesclosing;
        $listing->wedopening = $request->wedopening;
        $listing->wedclosing = $request->wedclosing;
        $listing->thursopening = $request->thursopening;
        $listing->thursclosing = $request->thursclosing;
        $listing->fridayopening = $request->fridayopening;
        $listing->fridayclosing = $request->fridayclosing;
        $listing->surtopening = $request->surtopening;
        $listing->surtclosing = $request->surtclosing;
        $listing->sunopening = $request->sunopening;
        $listing->sunclosing = $request->sunclosing;
        $listing->category_id = $request->category_id;
        $listing->area_id = $request->area_id;
        $listing->user()->associate($request->user());
         
        if($request->get('image_names') != '')
        {
            $listing->images = preg_split('/\|/', $request->get('image_names'), null, PREG_SPLIT_NO_EMPTY);
        }

        $listing->live = false;

        $listing->save();

        return redirect()->route('listings.edit', [$area, $listing]);
    }

    public function edit(Request $request, Area $area, Listing $listing)
    {
        $this->authorize('edit', $listing);
        
        return view('listings.v2edit', compact('listing'));
    }

    public function update(StoreListingFormRequest $request, Area $area, Listing $listing)
    {
        $this->authorize('update', $listing);

        $listing->title = $request->title;
        $listing->body = $request->body;
        $listing->companyname = $request->companyname;
         $listing->companyemail = $request->companyemail;
        $listing->address = $request->address;
        $listing->facebook = $request->facebook;
        $listing->twitter = $request->twitter;
        $listing->instagram = $request->instagram;
        $listing->website = $request->website;
        $listing->phone1 = $request->phone1;
        $listing->phone2 = $request->phone2;
        $listing->phone3 = $request->phone3;
        $listing->body = $request->body;
        $listing->monopening = $request->monopening;
        $listing->monclosing = $request->monclosing;
        $listing->tuesopening = $request->tuesopening;
        $listing->tuesclosing = $request->tuesclosing;
        $listing->wedopening = $request->wedopening;
        $listing->wedclosing = $request->wedclosing;
        $listing->thursopening = $request->thursopening;
        $listing->thursclosing = $request->thursclosing;
        $listing->fridayopening = $request->fridayopening;
        $listing->fridayclosing = $request->fridayclosing;
        $listing->surtopening = $request->surtopening;
        $listing->surtclosing = $request->surtclosing;
        $listing->sunopening = $request->sunopening;
        $listing->sunclosing = $request->sunclosing;
        if (!$listing->live()) {
            $listing->category_id = $request->category_id;
        }

        $listing->area_id = $request->area_id;

         if($request->get('image_names') != '')
        {
           
            $images = preg_split('/\|/', $request->get('image_names'), null, PREG_SPLIT_NO_EMPTY);

            //Lets delete the removed images
            foreach ($listing->images as $image) {
                if(!in_array($image, $images)){

                    Storage::disk('public')->delete("listing-images/$listing->user_id/$image");
                }
            }

            $listing->images = $images;
        }

        $listing->save();

        if ($request->has('payment')) {
            return redirect()->route('listings.payment.show', [$area, $listing]);
        }
        
        return back()->withSuccess('Listing edited successfully.');
    }

    public function destroy(Area $area, Listing $listing)
    {
        $this->authorize('destroy', $listing);

        $listing->delete();

        return back()->withSuccess('Listing was deleted.');
    }
 /**
     * Search for listings by category and keywords.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request, Area $area)
    {

        
        $keyword = $request->get('query');

    
        if(empty($keyword))
        {
            //we'll return an empty listing collection if no category or keyword
            $listings = collect(new Listing);
            return view('search')->with(compact('listings'));
        }


      


        

        $listings = Listing::SearchByKeyword($keyword)->paginate(20);

        return view('search')->with(compact('listings'));
    }

}