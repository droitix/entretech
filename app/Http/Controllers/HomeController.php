<?php

namespace App\Http\Controllers;

use App\{Area, Category, Listing};
use Illuminate\Http\Request;

class HomeController extends Controller
{
      const INDEX_LIMIT = 10;

    public function index(Area $area, Category $category, Listing $listing)
    {
        $areas = Area::get()->toTree();

        $listings = \App\Listing::fromlodgesCategory($category)->paginate(4);

        $schools = \App\Listing::fromvarCategory($category)->paginate(4);

        $restaurants = \App\Listing::fromresCategory($category)->paginate(4);

        return view('v2landing', compact('areas','listings','schools','restaurants'));
    }
}
